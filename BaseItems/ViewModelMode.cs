﻿namespace MVVMTools.BaseItems
{
    public enum ViewModelMode
    {
        Default,
        ReadOnly,
        Edit
    }
}