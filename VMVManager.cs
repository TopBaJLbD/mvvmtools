﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using MVVMTools.BaseItems;

namespace MVVMTools
{
    /// <summary>
    /// Менеджер моделей MVVM
    /// Осуществляет связь между View и ViewModel
    /// </summary>
    public static class VMVManager
    {
        /// <summary>
        /// Инициализирует менеджер MVVM
        /// </summary>
        public static void Initialise()
        {
            lock (mSyncLock)
            {
                if(IsInitialised)
                    return;
                mWindowTypes = new List<Type> {typeof (Window)};
                mViewModels = new List<WorkspaceViewModel>();
                mViews = new List<Control>();
                
                var asms = AppDomain.CurrentDomain.GetAssemblies();
                var typesToSearch = (from a in asms
                                     where a.GlobalAssemblyCache == false && a.IsDynamic == false
                                     from t in a.GetExportedTypes()
                                     select t).ToList().Distinct();
                
                //  Find every type that has the View attribute.
                mViewTypes = from t in typesToSearch
                             where t.GetCustomAttributes(typeof (ViewModelModeAttribute), false).Any()
                             select t;
                
                foreach (var viewType in mViewTypes)
                    RegisterView(viewType);

                IsInitialised = true;
            }
        }

        private static IEnumerable<Type> mViewTypes;
        private static IList<WorkspaceViewModel> mViewModels;
        private static IList<Control> mViews;

        private static List<Type> mWindowTypes;

        /// <summary>
        /// Добавляет тип, который будет использоваться как Window
        /// </summary>
        /// <param name="windowType"></param>
        public static void RegisterWindowType(Type windowType)
        {
            if(mWindowTypes.Contains(windowType))
                return;
            mWindowTypes.Add(windowType);
        }

        /// <summary>
        /// Указывает зарегистрирован ли данный тип в качестве окна
        /// </summary>
        /// <param name="wt"></param>
        /// <returns></returns>
        public static bool IsWindowRegistered(Type wt)
        {
            return mWindowTypes.Any(windowType => windowType.IsAssignableFrom(wt));
        }

        /// <summary>
        /// A map of View types to ViewModel types.
        /// </summary>
        private static readonly IList<ViewViewModelMapping> mViewViewModelMappings = new List<ViewViewModelMapping>();

        private static void RegisterView(Type viewType)
        {
            var attr = viewType.GetCustomAttributes(false).FirstOrDefault(at =>
                                                                              {
                                                                                  var a = at as ViewModelModeAttribute;
                                                                                  return a != null;
                                                                              }) as ViewModelModeAttribute;
            if(attr!=null)
            {
                mViewViewModelMappings.Add(new ViewViewModelMapping
                                               {
                                                   ViewType = viewType,
                                                   ViewModelType = attr.ViewModelType,
                                                   Mode = attr.Mode,
                                                   IsDefault = attr.IsDefault
                                               });
            }
        }

        /// <summary>
        /// Находит ViewModel или создаёт её (конструктор по умолчанию)
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="id">Идентификатор</param>
        /// <param name="parent">Родительская модель</param>
        /// <returns></returns>
        private static WorkspaceViewModel ResolveViewModel<T>(Guid? id, WorkspaceViewModel parent) where T : class , new()
        {
            EnsureInitialised();

            // TODO Добавить разграничение по типу, для одного Id может быть много разных моделей и представлений!
            // Хотя может я и не прав)
            if (id.HasValue)
            {
                var viewModel = mViewModels.Where( w => w.GetType() == typeof(T)).FirstOrDefault(w=>w.Id==id);
                if (viewModel != null)
                {
                    viewModel.Init(id.Value);
                    return viewModel;
                }
            }
            else
            {
                var viewModel = mViewModels.OfType<T>().FirstOrDefault();
                if (viewModel != null)
                {
                    return viewModel as WorkspaceViewModel;
                }
            }

            var ret = Activator.CreateInstance(typeof(T)) as WorkspaceViewModel;

            if (ret == null)
                throw new ArgumentException();
            
            ret.Parent = parent;
            mViewModels.Add(ret);
            return ret;
        }

        /// <summary>
        /// Показывает окно для указанной модели
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mode"></param>
        public static void ShowWindow(WorkspaceViewModel model, ViewModelMode mode = ViewModelMode.Default)
        {
            EnsureInitialised();
            var window = FindView(model, mode);
            if(window!=null)
            {
                if (WindowAction != null)
                {
                    WindowAction(window);
                }
                else
                {
                    InternalShowWindow(window as Window);
                }
            }
        }

        /// <summary>
        /// Свойство делегат отображения окна.
        /// </summary>
        public static Action<Control> WindowAction { get; set; }

        /// <summary>
        /// Свойство делегат отображения окна диалога.
        /// </summary>
        public static Func<Control, bool> DialogAction { get; set; }


        /// <summary>
        /// Показывает View для модели в виде диалога
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static bool ShowDialog(WorkspaceViewModel model, ViewModelMode mode = ViewModelMode.Default)
        {
            EnsureInitialised();
            var window = FindView(model, mode);
            if (window != null)
            {
                if(DialogAction!=null)
                {
                    return DialogAction(window);
                }
                return InternalShowDialog(window as Window);

            }
            return false;
        }

        private static void InternalShowWindow(Window window)
        {
            if(window == null)
                return;

            if (window.WindowState == WindowState.Minimized) window.WindowState = WindowState.Normal;
            window.Show();
        }

        private static bool InternalShowDialog(Window window)
        {
            if (DialogAction != null)
                return DialogAction(window);
            
            return window.ShowDialog() == true;
        }

        private static Control FindView(WorkspaceViewModel model, ViewModelMode mode)
        {
            // 1. Если view для данной модели уже существует то отображаем её
            var view = mViews.FirstOrDefault(v => v.DataContext == model);
            if (view != null)
            {
                SetOwner(view, model.Parent);
                return view;
            }
            // 2. Находим тип view для данной модели
            var type = FindViewType(model.GetType(), mode);
            if (type == null)
                throw new Exception(
                    string.Format("Для модели {0} не зарегестрировано View!", model.GetType()));


            var control = Activator.CreateInstance(type.ViewType) as Control;
            if (control != null)
            {
                control.DataContext = model;
                // TODO 
                SetOwner(control, model.Parent);
                //SetOwner(control, model);
                mViews.Add(control);
                return control;
            }
            return null;
        }

        /// <summary>
        /// Закрывает все view при закрытии модели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal static void RetRequestClose(object sender, RequestCloseEventArgs e)
        {
            var model = sender as BaseViewModel;
            if (model == null) return;

            var views = mViews.Where(el => el.DataContext == model).ToList();
            var q = new Queue<Control>(views);

            while (q.Count != 0)
            {
                var ctl = q.Dequeue();
                mViews.Remove(ctl);
                var hasWindow = HasWindow(ctl);
                if (!hasWindow)
                    continue;

                var wtype = ctl.GetType();
                if (IsWindowRegistered(wtype))
                {
                    // Окно уже может быть закрыто тогда нафиг тут вызов???
                    try
                    {
                        if (ComponentDispatcher.IsThreadModal)
                        {
                            var property = wtype.GetProperty("DialogResult");
                            property.SetValue(ctl, e.DialogResult, null);
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        var closeMethod = wtype.GetMethod("Close");
                        closeMethod.Invoke(ctl, new object[] {});
                    }
                }
            }
        }
        
        /// <summary>
        /// Возвращает модель для указанного Control
        /// </summary>
        /// <param name="ctl"></param>
        /// <returns></returns>
        public static BaseViewModel GetModel(Control ctl)
        {
            EnsureInitialised();
            return ctl == null ? null : ctl.DataContext as BaseViewModel;
        }

        /// <summary>
        /// Добавляет родительское окно указанному элементу управления
        /// </summary>
        /// <param name="window"></param>
        /// <param name="parent"></param>
        private static void SetOwner(Control window, BaseViewModel parent)
        {
            var wtype = window.GetType();
            var property = wtype.GetProperty("Owner");
            if (IsWindowRegistered(wtype))
            {
                var needSet = true;
                if (parent != null)
                {
                    var view = FindView(parent);
                    if (view != null)
                    {
                        var w = GetControlWindow(view);
                        if (w != null && w != window)
                        {
                            property.SetValue(window, w, null);
                            needSet = false;
                        }
                    }
                }
                if(needSet)
                {
                    property.SetValue(window, ActiveWindow, null);
                }
            }
        }

        /// <summary>
        /// Возвращает View на которому соответствует модель.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Control FindView(BaseViewModel model)
        {
            EnsureInitialised();
            var c = mViews.FirstOrDefault(v => v.DataContext == model);
            if (c != null)
                return c;
            if(model !=null && model.Parent !=null)
                return FindView(model.Parent);
            return null;
        }

        /// <summary>
        /// Находит Window которому принадлежит Control
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private static Control GetControlWindow(Control control)
        {
            var ctl = control;
            while (ctl != null)
            {
                 var wtype = ctl.GetType();
                 if (IsWindowRegistered(wtype))
                     return ctl;

                ctl = ctl.Parent as Control;
            }
            return null;
        }

        private static IEnumerable<ViewViewModelMapping> FindViewTypes(Type modelType)
        {
            return mViewViewModelMappings.Where(map=>map.ViewModelType == modelType).ToList();
        }

        private static ViewViewModelMapping FindViewType(Type modelType, ViewModelMode mode)
        {
            var types = FindViewTypes(modelType).ToList();
            return types.FirstOrDefault(m => m.Mode == mode) ?? types.FirstOrDefault(m => m.IsDefault);
        }

        /// <summary>
        /// Ensures the manager is initialised.
        /// </summary>
        private static void EnsureInitialised()
        {
            //  If we're initialised, we're done.
            if (IsInitialised)
                return;

            //  Initialise.
            Initialise();
        }

        /// <summary>
        /// A sync lock for thread safety.
        /// </summary>
        private static readonly object mSyncLock = new object();

        /// <summary>
        /// A boolean to determine whether the manager is initialised.
        /// </summary>
        private static bool IsInitialised { get; set; }

        /// <summary>
        /// Возвращает текущее активное окно
        /// </summary>
        public static Window ActiveWindow
        {
            get
            {
                if(Application.Current!=null)
                {
                    return Application.Current.Windows.OfType<Window>().FirstOrDefault(w => w.IsActive);

                }
                return null;
            }
        }

        /// <summary>
        /// Возвращает true если данное окно существует в списке окон приложения
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        public static bool HasWindow(Control window)
        {
            if(Application.Current!=null)
            {
                return Application.Current.Windows.OfType<Window>().Contains(window);
            }
            return false;
        }

        /// <summary>
        /// Внутреннний класс, хранящий информацию о связях View и ViewModel
        /// </summary>
        private class ViewViewModelMapping
        {
            /// <summary>
            /// Тип ViewModel
            /// </summary>
            public Type ViewModelType { get; set; }

            /// <summary>
            /// Тип View
            /// </summary>
            public Type ViewType { get; set; }

            public ViewModelMode Mode { get; set; }

            public bool IsDefault { get; set; }
        }
    }
}
