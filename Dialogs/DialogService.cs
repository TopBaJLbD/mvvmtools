﻿using MVVMTools.BaseItems;

namespace MVVMTools.Dialogs 
{
    /// <summary>
    /// Сервис отображения дилогов
    /// </summary>
    public class DialogService
    {
        private static DialogService mInstance;
        private readonly DialogServiceViewModel mService;
        
        private DialogService()
        {
            mService = new DialogServiceViewModel();
        }

        private static void EnsureInitialized()
        {
            if(mInstance==null)
                mInstance = new DialogService();
        }

        /// <summary>
        /// Показывает сообщение
        /// </summary>
        /// <param name="title">Заголовок</param>
        /// <param name="message">Текст сообщения</param>
        /// <param name="dialogType">Тип диалога</param>
        private static void ShowDialog(string title, string message, DialogType dialogType)
        {
            EnsureInitialized();

            string dtitle;
            switch (dialogType)
            {
                case DialogType.Information:
                    dtitle = "Информация ...";
                    break;
                case DialogType.Warning:
                    dtitle = "Предупреждение ...";
                    break;
                case DialogType.Error:
                    dtitle = "Ошибка ...";
                    break;
                default:
                    dtitle = "Сообщение ...";
                    break;
            }

            var model = new DialogBoxViewModel(title, message, dialogType);
            ShowDialog(dtitle, model);
        }


        /// <summary>
        /// Показывает сообщение
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void ShowMessage(string title, string message)
        {
            ShowDialog(title, message, DialogType.None);
        }

        /// <summary>
        /// Показывает информационное сообщение
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void ShowInfo(string title, string message)
        {
            ShowDialog(title, message, DialogType.Information);
        }

        /// <summary>
        /// Показывает сообщение предупреждение
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void ShowWarning(string title, string message)
        {
            ShowDialog(title, message, DialogType.Warning);
        }

        /// <summary>
        /// Показывает сообщение об ошибке
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void ShowError(string title, string message)
        {
            ShowDialog(title, message, DialogType.Error);
        }

        /// <summary>
        /// Отображает диалог для модели
        /// </summary>
        /// <param name="title">Заголовок окна</param>
        /// <param name="dialogModel"></param>
        public static void ShowDialog(string title, WorkspaceViewModel dialogModel)
        {
            EnsureInitialized();
            mInstance.mService.DisplayName = title;
            dialogModel.Parent = mInstance.mService;
            dialogModel.RequestClose += ModelRequestClose;
            mInstance.mService.AddWorkspace(dialogModel, true);
            VMVManager.ShowDialog(mInstance.mService);
        }

        private static void ModelRequestClose(object sender, RequestCloseEventArgs e)
        {
            // Делаем скрытие окна
            var view = VMVManager.FindView(mInstance.mService);
            if(view == null)
                return;

            var wtype = view.GetType();
            var hideMethod = wtype.GetMethod("Hide");
            if(hideMethod!=null)
                hideMethod.Invoke(view, new object[] { });
        }

    }
}
