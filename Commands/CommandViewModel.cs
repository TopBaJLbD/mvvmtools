﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using MVVMTools.BaseItems;

namespace MVVMTools.Commands
{
    /// <summary>
    /// Модель отображения кнопок
    /// </summary>
    public class CommandViewModel : BaseViewModel
    {
        /// <summary>
        /// Ctor.
        /// </summary> 
        /// <param name="command"></param>
        /// <param name="displayName"></param>
        /// <param name="imagePath"></param>
        /// <param name="path"></param>
        /// <param name="horizontalAlignment"></param>
        public CommandViewModel(ICommand command, string displayName, string imagePath = "", Path path = null, HorizontalAlignment horizontalAlignment = HorizontalAlignment.Left)
        {
            if (command == null)
                throw new ArgumentNullException("command");
            Command = command;
            DisplayName = displayName;
            Description = null;
            ImagePath = imagePath;
            Path = path;
            HorizontalAlignment = horizontalAlignment;
        }
       
        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Данные, связанные с моделью
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// Путь к изображению
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// Команда
        /// </summary>
        public ICommand Command { get; private set; }

        /// <summary>
        /// Графическая фигура
        /// </summary>
        public Path Path { get; private set; }

        /// <summary>
        /// Признак выбора
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// Горизонтальное выравнивание
        /// </summary>
        public HorizontalAlignment HorizontalAlignment { get; private set; }
        
    }
}
