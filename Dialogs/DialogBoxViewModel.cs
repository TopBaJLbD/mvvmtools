﻿using MVVMTools.BaseItems;

namespace MVVMTools.Dialogs
{
    /// <summary>
    /// Модель для содержимого стандартных диалогов
    /// </summary>
    public class DialogBoxViewModel:WorkspaceViewModel 
    {
        private DialogType mType;
        
        /// <summary>
        /// Тип диалога
        /// </summary>
        public DialogType DialogType
        {
            get { return mType; }
            set
            {
                mType = value;
                OnPropertyChanged("DialogType");
            }
        }

        private string mMessage;

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Message
        {
            get { return mMessage; }
            set
            {
                mMessage = value;
                OnPropertyChanged("Message");
            }
        }

        /// <summary>
        /// Ctro.
        /// </summary>
        /// <param name="title">Заголовок</param>
        /// <param name="message">Сообщение</param>
        /// <param name="dialogType">Тип диалога</param>
        public DialogBoxViewModel(string title, string message, DialogType dialogType)
        {
            DisplayName = title;
            Message = message;
            DialogType = dialogType;
        }


    }
}
