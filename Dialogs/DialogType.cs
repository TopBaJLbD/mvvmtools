﻿namespace MVVMTools.Dialogs 
{
    /// <summary>
    /// Тип диалога
    /// </summary>
    public enum DialogType
    {
        /// <summary>
        /// Без типа
        /// </summary>
        None,
        /// <summary>
        /// Информация
        /// </summary>
        Information,
        /// <summary>
        /// Предупреждение
        /// </summary>
        Warning,
        /// <summary>
        /// Ошибка
        /// </summary>
        Error
    }
}
