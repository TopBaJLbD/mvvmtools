﻿using MVVMTools.BaseItems;

namespace MVVMTools.Dialogs 
{
    /// <summary>
    /// Класс окна дилога
    /// </summary>
    public class DialogServiceViewModel:WorkspaceViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public DialogServiceViewModel()
        {
            RequestClose += DialogServiceViewModelRequestClose;
        }

        private void DialogServiceViewModelRequestClose(object sender, RequestCloseEventArgs e)
        {
            //Clean Workspaces on closing
            Workspaces.Clear();
        }
    }
}
