﻿using System;
using System.Windows;
using System.Windows.Controls;
using MVVMTools.Commands;

namespace MVVMTools.View
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:Neolant.RWNPP.View.Windows"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:Neolant.RWNPP.View.Windows;assembly=Neolant.RWNPP.View.Windows"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:CommandView/>
    ///
    /// </summary>
    public class CommandView : Control
    {
        static CommandView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CommandView), new FrameworkPropertyMetadata(typeof(CommandView)));
        }

        public CommandView()
        {
            Initialized += CtlCommandInitialized;

        }

        public ControlTemplate TextTemplate
        {
            get { return (ControlTemplate)GetValue(TextTemplateProperty); }
            set { SetValue(TextTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextTemplateProperty =
            DependencyProperty.Register("TextTemplate", typeof(ControlTemplate), typeof(CommandView), new UIPropertyMetadata(null));

        public ControlTemplate ImageTemplate
        {
            get { return (ControlTemplate)GetValue(ImageTemplateProperty); }
            set { SetValue(ImageTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageTemplateProperty =
            DependencyProperty.Register("ImageTemplate", typeof(ControlTemplate), typeof(CommandView), new UIPropertyMetadata(null));



        void CtlCommandInitialized(object sender, EventArgs e)
        {
            var command = DataContext as CommandViewModel;
            if (command == null) return;
            Template = !string.IsNullOrEmpty(command.ImagePath) ? ImageTemplate : TextTemplate;
            ApplyTemplate();
        }
    }
}
