﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace MVVMTools.BaseItems
{
    /// <summary>
    /// Base class for all ViewModel classes in the application.
    /// It provides support for property change notifications 
    /// and has a DisplayName property.  This class is abstract.
    /// </summary>
    public abstract class BaseViewModel : DependencyObject, INotifyPropertyChanged, IDataErrorInfo, IDisposable
    //, IDisposable
    {

        private string mDisplayName;

        /// <summary>
        /// Returns the user-friendly name of this object.
        /// Child classes can set this property to a new value,
        /// or override it to determine the value on-demand.
        /// </summary>
        public string DisplayName
        {
            get { return mDisplayName; }
            set
            {
                mDisplayName = value;
                OnPropertyChanged("DisplayName");
            }
        }

        /// <summary>
        /// Родительская модель
        /// </summary>
        public BaseViewModel Parent { get; set; }

        /// <summary>
        /// Самая верхняя модель родителя
        /// </summary>
        public BaseViewModel TopParent
        {
            get
            {
                var parent = this;
                do
                {
                    parent = parent.Parent;

                } while (parent.Parent != null);


                return parent;
            }
        }

        /// <summary>
        /// Идентификатор модели
        /// </summary>
        public Guid Id { get; protected set; }

        /// <summary>
        /// Инициализация модели
        /// </summary>
        /// <param name="id"></param>
        public virtual void Init(Guid id)
        {
        }


        /// <summary>
        /// Warns the developer if this object does not have
        /// a public property with the specified name. This 
        /// method does not exist in a Release build.
        /// </summary>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            var noProperty = GetType().GetProperty(propertyName) == null;

            //NOTE TypeDescriptor почему-то выдает ошибку "Ambiguous match" для открытых свойств наследуемых классов, 
            //например класс BaseTreeItem содержит открытое свойство Childs, которое TypeDescriptor не находит у его наследника, например OrganizationItem
            //if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            if (noProperty && ThrowOnInvalidPropertyName)
            {
                var msg = "Недопустимое имя свойства: " + propertyName;
                throw new Exception(msg);
            }
        }

        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; set; }


        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        private PropertyChangedEventHandler mPropertyChanged;

        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                var changedEventHandler = mPropertyChanged;
                PropertyChangedEventHandler comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange(ref mPropertyChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                var changedEventHandler = mPropertyChanged;
                PropertyChangedEventHandler comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange(ref mPropertyChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }


        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            VerifyPropertyName(propertyName);
            var changedEventHandler = mPropertyChanged;
            if (changedEventHandler == null)
                return;
            var e = new PropertyChangedEventArgs(propertyName);
            changedEventHandler(this, e);
        }

        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            OnPropertyChanged(((MemberExpression)propertyExpression.Body).Member.Name);
        }

        public virtual string this[string columnName]
        {
            get { return null; }
        }

        public virtual string Error
        {
            get { return null; }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Вызывает событие изменения свойства
        /// </summary>
        /// <param name="propertyName"></param>
        public void RaisePropertyChanged(string propertyName)
        {
            OnPropertyChanged(propertyName);
        }

        /// <summary>
        /// Вызов в потоке UI
        /// </summary>
        /// <param name="action"></param>
        public static void InvokeOnUIThread(Action action)
        {
            var currentDispatcher = Dispatcher.CurrentDispatcher;
            if (currentDispatcher.CheckAccess())
                action();
            else
                currentDispatcher.BeginInvoke(action, new object[0]);
        }

#if DEBUG
        // <summary>
        // Useful for ensuring that ViewModel objects are properly garbage collected.
        // </summary>
        //~BaseViewModel()
        //{
            //string msg = string.Format("{0} ({1}) ({2}) Удалено", GetType().Name, DisplayName, GetHashCode());
            //Debug.WriteLine(msg);
        //}
#endif
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
