﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace MVVMTools.Commands
{
    /// <summary>
    /// Базовый класс реализующий команды
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        readonly Action<object> mExecute;
        readonly Predicate<object> mCanExecute;

        /// <summary>
        /// Вызывается при завершении комманды
        /// </summary>
        public event Action Completed = delegate { }; 

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute = null)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            mExecute = execute;
            mCanExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough] 
        public bool CanExecute(object parameter)
        {
            return mCanExecute == null || mCanExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            mExecute(parameter);
            Completed();
        }

        #endregion // ICommand Members 
    }
}
