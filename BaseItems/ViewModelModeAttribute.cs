﻿using System;

namespace MVVMTools.BaseItems
{
    public class ViewModelModeAttribute : Attribute
    {
        private readonly Type mViewModelType;
        private readonly ViewModelMode mMode;
        private readonly bool mIsDefault;

        public ViewModelModeAttribute(Type viewModelType, ViewModelMode mode, bool isDefault = false)
        {
            if (mode == ViewModelMode.Default)
                throw new ArgumentException("Mode can't be set to Default value", "mode");
            mViewModelType = viewModelType;
            mMode = mode;
            mIsDefault = isDefault;
        }

        public Type ViewModelType
        {
            get { return mViewModelType; }
        }

        public ViewModelMode Mode
        {
            get { return mMode; }
        }

        public bool IsDefault
        {
            get { return mIsDefault; }
        }
    }
}