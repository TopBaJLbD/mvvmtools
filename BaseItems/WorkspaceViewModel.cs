﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using MVVMTools.Commands;

namespace MVVMTools.BaseItems
{
    /// <summary>
    /// Событие на закрытие диалога
    /// </summary> 
    public class RequestCloseEventArgs : EventArgs
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="dialogResult"></param>
        public RequestCloseEventArgs(bool dialogResult)
        {
            DialogResult = dialogResult;
        }

        /// <summary>
        /// Результат выполнения диалога
        /// </summary>
        public bool DialogResult { get; private set; }
    }

    /// <summary>
    /// This ViewModelBase subclass requests to be removed 
    /// from the UI when its CloseCommand executes, and MinimizeCommand
    /// This class is abstract.
    /// </summary>
    public abstract class WorkspaceViewModel : BaseViewModel
    {
        private RelayCommand mCloseCommand;
        private RelayCommand mCancelCommand;
        private RelayCommand mMinimizeCommand;
        private RelayCommand mRollUpCommand;
        private RelayCommand mMaximizedCommand;


        /// <summary>
        /// Запрос на 
        /// </summary>
        public event EventHandler RequestRollUp;

        /// <summary>
        /// Запрос на полный экран
        /// </summary>
        public event EventHandler RequestMaximized;

        /// <summary>
        /// Raised when this workspace cacshould be removed from the UI.
        /// </summary>
        public event EventHandler RequestCancel;

        /// <summary>
        /// Raised when this workspace should be removed from the UI.
        /// </summary>
        public event EventHandler RequestMinimize;

        /// <summary>
        /// Raised when this workspace should be removed from the UI.
        /// </summary>
        public event EventHandler<RequestCloseEventArgs> RequestClose = VMVManager.RetRequestClose;
        
        private ObservableCollection<CommandViewModel> mCommands;
        private ObservableCollection<WorkspaceViewModel> mWorkspaces;


        private string mStatusText;
        private string mStatusInfo;

        /// <summary>
        /// Текст статуса
        /// </summary>
        public string StatusText
        {
            get { return mStatusText; }
            protected set
            {
                mStatusText = value;
                OnPropertyChanged("StatusText");
            }
        }

        /// <summary>
        /// Информация о статусе
        /// </summary>
        public string StatusInfo
        {
            get { return mStatusInfo; }
            protected set
            {
                mStatusInfo = value;
                OnPropertyChanged("StatusInfo");
            }
        }

        /// <summary>
        /// Результат выполнения
        /// </summary>
        public bool DialogResult { get; set; }


        protected virtual IEnumerable<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>();
        }

        /// <summary>
        /// Returns a read-only list of commands 
        /// that the UI can display and execute.
        /// </summary>
        public ObservableCollection<CommandViewModel> Commands
        {
            get { return mCommands ?? (mCommands = new ObservableCollection<CommandViewModel>(CreateCommands())); }
        }

        /// <summary>
        /// Индикатор возможности отображения команд
        /// </summary>
        public Visibility CommandsVisibility
        {
            get { return Commands.Count > 0 ? Visibility.Visible : Visibility.Collapsed; }
        }

        //public CornerRadius AreaRadius
        //{
        //    get { return Commands.Count > 0 ? new CornerRadius(0, 0, 6, 6) : new CornerRadius(6); }
        //}


        /// <summary>
        /// Returns the collection of available workspaces to display.
        /// A 'workspace' is a ViewModel that can request to be closed.
        /// </summary>
        public ObservableCollection<WorkspaceViewModel> Workspaces
        {
            get
            {
                if (mWorkspaces == null)
                {
                    mWorkspaces = new ObservableCollection<WorkspaceViewModel>();
                    mWorkspaces.CollectionChanged += OnWorkspacesChanged;
                }
                return mWorkspaces;
            }
        }

        private void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.NewItems)
                {
                    workspace.RequestClose += OnWorkspaceRequestClose;
                }

            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.OldItems)
                {
                    workspace.RequestClose -= OnWorkspaceRequestClose;
                }
        }

        private void OnWorkspaceRequestClose(object sender, RequestCloseEventArgs e)
        {
            var workspace = sender as WorkspaceViewModel;
            if (workspace != null)
            {
                Workspaces.Remove(workspace);
            }
        }

        protected void SetActiveWorkspace(WorkspaceViewModel workspace)
        {
            var collectionView = CollectionViewSource.GetDefaultView(Workspaces);
            if (collectionView != null)
                collectionView.MoveCurrentTo(workspace);
            
            foreach (var wsp in Workspaces)
                workspace.IsActive = Equals(wsp, workspace);

            OnPropertyChanged("ActiveWorkspace");
            OnPropertyChanged("ActiveIndex");
        }

        /// <summary>
        /// Активная модель
        /// </summary>
        public WorkspaceViewModel ActiveWorkspace
        {
            get
            {
                var collectionView = CollectionViewSource.GetDefaultView(Workspaces);
                if (collectionView != null)
                    return collectionView.CurrentItem as WorkspaceViewModel;

                return null;
            }
            set
            {
                SetActiveWorkspace(value);
            }
        }

        /// <summary>
        /// Индекс активной модели
        /// </summary>
        public int ActiveIndex
        {
            get { return Workspaces.IndexOf(ActiveWorkspace); }
            set
            {
                if(value>-1 && value<Workspaces.Count())
                {
                    var wrk = Workspaces[value];
                    SetActiveWorkspace(wrk);
                }
            }
        }

        /// <summary>
        /// Возвращает <c>true</c> если в коллекции есть модель указаннного типа
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool HasWorkspace(Type type)
        {
            return GetWorkspace(type) != null;
        }

        /// <summary>
        /// Возвращает первую найденную модель указанного типа
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public WorkspaceViewModel GetWorkspace(Type type)
        {
            return Workspaces.FirstOrDefault(vm => vm.GetType() == type);
        }

        /// <summary>
        /// Добавляет и активирует модель
        /// </summary>
        /// <param name="workspace"></param>
        /// <param name="activate"> </param>
        public void AddWorkspace(WorkspaceViewModel workspace, bool activate)
        {
            if (!Workspaces.Contains(workspace))
                Workspaces.Add(workspace);

            if (activate)
                SetActiveWorkspace(workspace);
        }

        /// <summary>
        /// Добавляет модель и активирует только 1 модель коллекции
        /// </summary>
        /// <param name="workspace"></param>
        public void AddWorkspace(WorkspaceViewModel workspace)
        {
            if (!Workspaces.Contains(workspace))
                Workspaces.Add(workspace);

            if (Workspaces.Count == 1)
                SetActiveWorkspace(workspace);
        }


        /// <summary>
        /// Выбрана ли эта viewmodel, если она присутствует списке, который биндинтся к контролу с выбором элемента
        /// </summary>
        public bool IsActive
        {
            get
            {
                return (bool)GetValue(IsActiveProperty);
            }
            set
            {
                SetValue(IsActiveProperty, value);
            }
        }

        /// <summary>
        /// Свойство IsActive
        /// </summary>
        public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register("IsActive", typeof(bool), typeof(WorkspaceViewModel), new UIPropertyMetadata(null));

        /// <summary>
        /// Returns the command that, when invoked, attempts
        /// to remove this workspace from the user interface.
        /// </summary>
        public ICommand CloseCommand
        {
            get { return mCloseCommand ?? (mCloseCommand = new RelayCommand(param => OnRequestClose())); }
        }

        /// <summary>
        /// Returns the command that, when invoked, attempts
        /// to cancel and remove this workspace from the user interface.
        /// </summary>
        public ICommand CancelCommand
        {
            get { return mCancelCommand ?? (mCancelCommand = new RelayCommand(param => OnCancel())); }
        }

        /// <summary>
        /// Returns the command that, when invoked, attempts
        /// to minimaize this workspace from the user interface.
        /// </summary>
        public ICommand MinimizeCommand
        {
            get { return mMinimizeCommand ?? (mMinimizeCommand = new RelayCommand(param => OnRequestMinimize())); }
        }


        /// <summary>
        /// Свернуть рабочую область
        /// </summary>
        public ICommand RollUpCommand
        {
            get { return mRollUpCommand ?? (mRollUpCommand =new RelayCommand(param => OnRequestRollUp())); }
        }

        /// <summary>
        /// Развернуть на весь экран рабочую область
        /// </summary>
        public ICommand MaximizedCommand
        {
            get { return mMaximizedCommand ?? (mMaximizedCommand = new RelayCommand(param => OnRequestMaximized())); }
        }

        private void OnCancel()
        {
            if (RequestCancel != null)
                RequestCancel(this, EventArgs.Empty);
        }

        protected void OnRequestClose()
        {
            if (RequestClose != null)
                RequestClose(this, new RequestCloseEventArgs(DialogResult));
        }

        private void OnRequestMinimize()
        {
            if (RequestMinimize != null)
                RequestMinimize(this, EventArgs.Empty);
        }

        private void OnRequestRollUp()
        {
            if (RequestRollUp != null)
                RequestRollUp(this, EventArgs.Empty);
        }

        private void OnRequestMaximized()
        {
            if (RequestMaximized != null)
                RequestMaximized(this, EventArgs.Empty);
        }
       
    }
}